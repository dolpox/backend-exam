<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
Back-end Exam</p>

# Getting Started

## Installation

### API Server 
1. Clone this repository

        git clone git@gitlab.com:dolpox/backend-exam.git exam

2. Go to exam/api folder

        cd exam/api
   
3. Update .env with the right database credentials with your editor of choice.


4. Install composer dependencies 
    
        composer install

5. Run database migration
        
        php artisan migrate

6. Install passport auth.

        php artisan passport:install

7. Run the API server.
        
        php artisan serve


### Front-end server
1. Go to exam folder

        cd ../exam


2. Intall packages
    
        npm i

3. Start the server 

        npm start
