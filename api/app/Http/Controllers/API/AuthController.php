<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only(['logout']);
    }

    /**
     * Handles user registrations
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed'
        ]);

        $validatedData['password'] = Hash::make($request->password);

        $user = User::create($validatedData);

        return response()->json($user, 200);

    }

    /**
     * Login authentication
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required|exists:users',
            'password' => 'required'
        ]);

        if (!auth()->attempt($loginData)) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'password' => [
                        'The password field is invalid.'
                    ]
                ]
            ], 422);
        }

        $token = auth()->user()->createToken('authToken');
        $expiration = $token->token->expires_at->format('Y-m-d H:i:s');

        return response()->json([
            'token' => $token->accessToken,
            'token_type' => 'bearer',
            'expires_at' => $expiration,
        ]);
    }

    /**
     * Handles the user logout
     *
     * @param Request $request
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
    }
}
