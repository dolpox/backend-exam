<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use App\Models\Post;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse|AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        try {
            $post = Post::findBySlugOrFail($request->post);
            return CommentResource::collection($post->comments()->get());

        } catch (Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $post = Post::findBySlugOrFail($request->post);

        $validatedData = $request->validate([
            'body' => 'required',
        ]);

        $validatedData['commentable_type'] = 'App\Models\Post';
        $validatedData['post_id'] = $post->id;
        $validatedData['user_id'] = auth()->user()->id;
        $comment = Comment::create($validatedData);

        return response()->json($comment, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request)
    {
        try {
            $post = Post::findBySlugOrFail($request->post);
            $comment = $post->comments()->findOrFail($request->comment);
            $updatedComment = tap($comment)->update($request->all());

            $data = [
                'body' => $updatedComment->body,
                'commentable_type' => $updatedComment->commentable_type,
                'commentable_id' => $updatedComment->post_id,
                'creator_id' => $updatedComment->user_id,
                'parent_id' => null,
                'created_at' => $updatedComment->created_at,
                'updated_at' => $updatedComment->updated_at,
                'id' => $updatedComment->id
            ];

            return response()->json(['data' => $data], 200);

        } catch (Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            $post = Post::findBySlugOrFail($request->post);
            $comment = $post->comments()->findOrFail($request->comment);
            $comment->delete($request->comment);

            return response()->json(['status' => 'record deleted successfully'], 200);

        } catch (Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 404);
        }
    }
}
