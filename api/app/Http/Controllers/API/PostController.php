<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Models\Post;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Post $post
     * @return AnonymousResourceCollection
     */
    public function index(Request $request, Post $post)
    {
        if (auth()->guard('api')->check()) {
            $post = User::find($request->user('api')->id)->posts();
        }

        return PostResource::collection($post->paginate(15));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'content' => 'required',
            'image' => 'required'
        ]);

        $validatedData['user_id'] = auth()->user()->id;
        $post = Post::create($validatedData);

        return response()->json($post, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $slug
     * @return JsonResponse
     */
    public function show(Request $request, $slug)
    {
        try {
            if (auth()->check()) {
                $post = $request->user()->posts()->where('slug', $slug)->firstOrFail();
            } else {
                $post = Post::where('slug', $slug)->firstOrFail();
            }

            $data = [
                'id' => $post->id,
                'user_id' => $post->user_id,
                'title' => $post->title,
                'slug' => $post->slug,
                'image' => $post->image,
                'content' => $post->content,
                'created_at' => $post->created_at,
                'updated_at' => $post->updated_at,
                'deleted_at' => $post->deleted_at,
            ];

            return response()->json(['data' => $data], 200);

        } catch (Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $slug
     * @return JsonResponse
     */
    public function update(Request $request, $slug)
    {
        try {
            $post = Post::findBySlugOrFail($slug);
            $updatedPost = tap($post)->update($request->all());

            $data = [
                'id' => $updatedPost->id,
                'user_id' => $updatedPost->user_id,
                'title' => $updatedPost->title,
                'slug' => $updatedPost->slug,
                'content' => $updatedPost->content,
                'created_at' => $updatedPost->created_at,
                'updated_at' => $updatedPost->updated_at,
                'deleted_at' => $updatedPost->deleted_at,
            ];

            return response()->json(['data' => $data], 200);

        } catch (Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $slug
     * @return JsonResponse
     */
    public function destroy(Request $request, $slug)
    {
        try {
            Post::findBySlugOrFail($slug)->delete();
            return response()->json(['status' => 'record deleted successfully'], 200);

        } catch (Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 404);
        }
    }
}
