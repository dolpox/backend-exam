<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'body' => $this->body,
            'commentable_type' => $this->commentable_type,
            'commentable_id' => $this->post_id,
            'creator_id' => $this->user_id,
            'parent_id' => null,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'id' => $this->id
        ];
    }
}
