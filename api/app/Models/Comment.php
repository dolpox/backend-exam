<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Comment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id',
        'user_id',
        'body',
        'commentable_type',
    ];

    /**
     * Get the post that owns the comment.
     *
     * @return BelongsTo
     */
    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
